from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm

def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists": todolists
    }
    return render(request, "todos/todo_list_list.html", context)

def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todolist)
    context = {
        "todolist": todolist,
        "tasks": tasks,
        "totalitems": tasks.count(),  # Fix the total items count
    }
    return render(request, "todos/todo_list_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)  # Fix the redirection URL
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)

def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)  # Retrieve the TodoList instance
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todolist.id)  # Redirect to detail page
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "form": form,
        "todolist": todolist, 
        "id": id,
    }
    return render(request, "todos/todo_list_update.html", context)

def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")  # Redirect to the list view after deletion

    context = {
        "todolist": todolist,
    }
    return render(request, "todos/delete/todo_list_delete.html", context)