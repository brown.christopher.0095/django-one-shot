from django import forms
from todos.models import TodoList 
from django.forms import ModelForm

class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]